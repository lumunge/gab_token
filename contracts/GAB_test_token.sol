// SPDX-License-Identifier: MIT

pragma solidity 0.8.16;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract GABToken is ERC20 {
    constructor(uint256 initialSupply) ERC20("gabino", "GAB") {
        _mint(msg.sender, initialSupply);
    }
}