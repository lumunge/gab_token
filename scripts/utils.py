from brownie import network, accounts, config

LOCAL_BLOCKCHAIN_ENVS = ["development", "ganache-local", "hardhat"]

def get_account(index=None):
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVS:
        return accounts[0]
    if index:
        return accounts[index]
    if network.show_active() in config["networks"]:
        account = accounts.add(config["wallets"]["from_key"])
        return account
    return None