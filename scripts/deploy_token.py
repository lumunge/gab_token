from brownie import (
    GABToken,
    network,
    config,
)
from scripts.utils import get_account

supply = 10**24 # 1 million gabs

def deploy_gab():  # deploy gab token
    account = get_account()
    gab_address = GABToken.deploy(
        supply,
        {"from": account},
        publish_source=config["networks"][network.show_active()].get("verify"),
    )

    print(f"Contract Address: {gab_address}")

def main():
    deploy_gab()