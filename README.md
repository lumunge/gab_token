# Gabinos Faucet.

This faucet provides test GAB tokens for users of gabinos. While the GAB token has no real world value, it will allow GAB users to be able to create markets on the gabinos platform.

#### How to request GAB tokens from this faucet:

1. First, we have to provide our wallet address where we will receive the tokens.
2. If you need an increased rate limit, you can provide an API key that will be issued upon request.
3. Finally, click the **Receive GAB** button to receive GAB tokens.
4. The GAB tokens will be sent to your address in a matter of minutes.
5. We can now start creating markets and participating in the gabinos platform.
